/*
Minion
        Cost: 1
        Life: 2
        Damage: 1
        Range: 1
        Speed: 5
 
Artillery
        Cost: 9
        Life: 3
        Damage: 8
        Range: 9
        Speed: 2
 
Soldier
        Cost: 3
        Life: 6
        Damage: 2
        Range: 4
        Speed: 4
 
Tank
        Cost: 7
        Life: 9
        Damage: 6
        Range: 6
        Speed: 1
 
Moto
        Cost: 5
        Life: 4
        Damage: 4
        Range: 4
        Speed: 9
*/